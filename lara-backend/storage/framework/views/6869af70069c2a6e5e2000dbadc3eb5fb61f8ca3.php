<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Data Transactions</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3 mt-1">
                        <a href="<?php echo e(url('dashboard/campaign/create')); ?>" class="btn btn-sm btn-primary">
                            <i class="fas fa-plus"></i>
                            Tambah Data
                        </a>
                    </div>
                    <div>
                        <?php if(session('error')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session('error')); ?>

                        </div>
                        <?php endif; ?>

                        <?php if(session('success')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('success')); ?>

                        </div>
                        <?php endif; ?>
                    </div>
                    <hr>
                    <form action="<?php echo e(url('/dashboard/transactions?search=')); ?>" method="GET">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Cari transaksi.."
                                        name="search_campaign">
                                </div>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Campaign</th>
                                        <th scope="col">Users</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Waktu</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php ($i = 1); ?>
                                    <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <th scope="row"> <?php echo e($i); ?> </th>
                                            <td> <?php echo e($item->campaign_name); ?> </td>
                                            <td> <?php echo e($item->users_name); ?> </td>
                                            <td> Rp. <?php echo e(number_format($item->amount)); ?> </td>
                                            <td>
                                                <?php if($item->status == 'paid'): ?>
                                                <span class="badge badge-success">Paid</span>
                                                <?php endif; ?>

                                                <?php if($item->status == 'unpaid'): ?>
                                                <span class="badge badge-danger">Unpaid</span>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php echo e($item->created_at); ?>

                                            </td>
                                            <td>
                                                <?php if($item->status == 'unpaid'): ?>
                                                <a href="<?php echo e(url('/dashboard/transactions/approve/' . $item->id)); ?>" class="badge badge-success">Approve</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php ($i++); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/andri/Documents/Projects/donasi-online/lara-backend/resources/views/transactions/index.blade.php ENDPATH**/ ?>