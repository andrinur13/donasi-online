<!DOCTYPE html>
<html>
<head>
	<title>Campaign</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>

    <div class="row">
        <div class="col">
            <div class="text-center">
                Campaign Report
            </div>
        </div>
    </div>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Campaign</th>
				<th>Total Fund</th>
				<th>Current Fund</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1 ?>
			<?php $__currentLoopData = $campaign; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($i++); ?></td>
				<td><?php echo e($p->name); ?></td>
				<td>Rp. <?php echo e(number_format($p->goal_amount)); ?></td>
				<td>Rp. <?php echo e(number_format($p->current_amount)); ?></td>
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table>
 
</body>
</html><?php /**PATH /Users/andri/Documents/Projects/donasi-online/lara-backend/resources/views/campaign_pdf.blade.php ENDPATH**/ ?>