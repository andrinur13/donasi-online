<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Data Campaign</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3 mt-1">
                        <a href="<?php echo e(url('dashboard/campaign/create')); ?>" class="btn btn-sm btn-primary">
                            <i class="fas fa-plus"></i>
                            Tambah Data
                        </a>
                        <a href="<?php echo e(url('dashboard/campaign/report')); ?>" class="btn btn-sm btn-primary">
                            <i class="fas fa-download"></i>
                            Report
                        </a>
                    </div>
                    <div>
                        <?php if(session('error')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(session('error')); ?>

                        </div>
                        <?php endif; ?>

                        <?php if(session('success')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('success')); ?>

                        </div>
                        <?php endif; ?>
                    </div>
                    <hr>
                    <form action="<?php echo e(url('/dashboard/campaign?search=')); ?>" method="GET">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Cari campaign.."
                                        name="search_campaign">
                                </div>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Campaign</th>
                                        <th scope="col">Total Fund</th>
                                        <th scope="col">PIC</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php ($i = 1); ?>
                                    <?php $__currentLoopData = $campaign; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <th scope="row"> <?php echo e($i); ?> </th>
                                            <td> <?php echo e($item->name); ?> </td>
                                            <td> Rp. <?php echo e(number_format($item->goal_amount)); ?> </td>
                                            <td> <?php echo e($item->pic); ?> </td>
                                            <td>
                                                <a href="<?php echo e(url('/dashboard/campaign/delete/' . $item->id)); ?>" class="badge badge-danger">Delete</a>
                                                <a href="<?php echo e(url('/dashboard/campaign/edit/' . $item->id)); ?>" class="badge badge-warning">Update</a>
                                            </td>
                                        </tr>
                                        <?php ($i++); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/andri/Documents/Projects/donasi-online/lara-backend/resources/views/campaign/index.blade.php ENDPATH**/ ?>