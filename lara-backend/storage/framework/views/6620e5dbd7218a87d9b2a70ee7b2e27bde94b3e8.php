<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Data Campaign</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <form action="<?php echo e(url('/dashboard/campaign/update/' . $id)); ?>" method="POST">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name">Nama Campaign</label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo e($campaign->name); ?>">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="short_description">Deskripsi Singkat</label>
                                    <input type="text" class="form-control" id="short_description" name="short_description" value="<?php echo e($campaign->short_description); ?>">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <input type="text" class="form-control" id="description" name="description" value="<?php echo e($campaign->description); ?>">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="goal_amount">Total Fund</label>
                                    <input type="number" class="form-control" id="goal_amount" name="goal_amount" value="<?php echo e($campaign->goal_amount); ?>">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update Campaign</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/andri/Documents/Projects/donasi-online/lara-backend/resources/views/campaign/edit.blade.php ENDPATH**/ ?>